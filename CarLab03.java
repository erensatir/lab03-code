package trialpackage;

public class CarLab03 {
  private double odometer;
private String brand;
private String color;
private int gear;

public CarLab03(){
    odometer=0;
    brand="BMW";
    color="Red";
    gear=0;
}
    public CarLab03(String b,String c){
        odometer=0;
        brand=b;
        color=c;
        gear=0;
    }
    public CarLab03(String b,String c,int g){
        odometer=0;
        brand=b;
        color=c;
        gear=g;
    }
    public CarLab03(int g,String b,String c){
        odometer=0;
        brand=b;
        color=c;
        gear=g;
    }
    public CarLab03(String b,int g,String c){
        odometer=0;
        brand=b;
        color=c;
        gear=g;
    }
    public void implementGear(){
    gear=gear+1;
    }
    public void decrementGear(){
        gear-=1;
    }
    public void drive(int NOHT,int kmTPH){
    odometer+=(NOHT*kmTPH);
    }
}
